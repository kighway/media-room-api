# README
"nameless-beyond-68958" is the name of the original heroku app this was hosted on

this is a basic rails api only app

the pgsql database can add rooms, but the api is only meant to get patch requests from the media room

the raspberry pi in the media room points the heroku app that hosts this apir ("nameless-beyond-68958")

patch requests edit the open status of the room

there's no auth or security of any kind, this is just a prototype for testing time delays and user experience
