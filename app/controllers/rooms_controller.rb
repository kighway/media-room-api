class RoomsController < ApplicationController
  before_action :set_room

  def show
    render :json => @room.to_json(:only => [:name, :open_status])
  end

  def update
    if @room.update(open_status: params[:open_status])
      render :json => @room.to_json(:only => [:name, :open_status])
    else
      render json: @room.errors, status: :unprocessable_entity
    end
  end

  private
    def set_room
      @room = Room.find(1)
    end
end
