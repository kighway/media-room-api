Rails.application.routes.draw do
  get 'media', action: :show, controller: 'rooms'
  patch 'media', action: :update, controller: 'rooms'
end
